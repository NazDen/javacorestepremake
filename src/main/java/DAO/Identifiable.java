package DAO;

public interface Identifiable {
    int id();
    String toText();
}
