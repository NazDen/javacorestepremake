package DAO;


import java.util.Collection;

public interface DAO <T extends Identifiable> {

    void save (T object);
    T get(int id);
    Collection<T> getAll();
    void remove(int id);

    default void remove(T object){
        remove(object.id());
    }
}
