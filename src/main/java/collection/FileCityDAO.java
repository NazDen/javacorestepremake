package collection;

import DAO.DAO;
import IO.WorkInFile;
import entity.City;
import entity.CityCreator;

import java.io.*;
import java.util.*;

public class FileCityDAO implements DAO<City> {

    public void makeCities(){
        CityCreator cityCreator = new CityCreator();
        List<City> cities = cityCreator.createCities();
        StringBuilder stringBuilder = new StringBuilder();
        cities.forEach(city -> stringBuilder.append(city.toText()));
        WorkInFile.write("cities.txt", stringBuilder.toString());
    }

    //DAO
    @Override
    public void save(City city) {
        try {
            WorkInFile.update("cities.txt", city.toText());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public City get(int index) {
        System.out.println("IN PROVIDING...");
        return null;
    }

    @Override
    public Collection<City> getAll() {
        List<City> cities = new ArrayList<>();
        String readFromFile = null;
        try {
            readFromFile = WorkInFile.read("cities.txt");
            String[] arrStringFromFile = readFromFile.split("\n");
            Arrays.stream(arrStringFromFile).forEach(s -> cities.add(new City(s)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return cities;
    }

    @Override
    public void remove(int id) {
        System.out.println("IN PROVIDING...");
    }

    @Override
    public void remove(City city) {
        System.out.println("IN PROVIDING...");
    }
}
