package collection;

import DAO.DAO;
import entity.City;
import java.util.Collection;
import java.util.HashMap;

public class CollectionCityDAO implements DAO<City> {

    private final HashMap<Integer, City> storage = new HashMap<>();

    @Override
    public void save(City city) {
        storage.put(city.id(),city);
    }

    @Override
    public City get(int id) {
        return storage.get(id);
    }

    @Override
    public Collection<City> getAll() {
        return storage.values();
    }

    @Override
    public void remove(int id) {
        storage.remove(id);
    }
}
