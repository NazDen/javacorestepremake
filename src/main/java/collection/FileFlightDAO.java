package collection;

import DAO.DAO;
import IO.WorkInFile;
import entity.Flight;
import entity.FlightCreator;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class FileFlightDAO implements DAO<Flight> {

    public void makeFlights(){
        FlightCreator flightCreator = new FlightCreator();
        List<Flight> flights = flightCreator.createFlights();
        StringBuilder stringBuilder = new StringBuilder();
        flights.forEach(flight -> stringBuilder.append(flight.toText()));
        WorkInFile.write("flights.txt", stringBuilder.toString());
    }

    @Override
    public void save(Flight flight) {
        try {
            WorkInFile.update("flights.txt", flight.toText());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Flight get(int id) {
        System.out.println("IN PROVIDING...");
        return null;
    }

    @Override
    public Collection<Flight> getAll() {
        List<Flight> listOfFlights = new ArrayList<>();
        String readFromFile = null;
        try {
            readFromFile = WorkInFile.read("flights.txt");
            String[] arrStringFromFile = readFromFile.split("\n");
            Arrays.stream(arrStringFromFile).forEach(s -> listOfFlights.add(new Flight(s)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return listOfFlights;
    }

    @Override
    public void remove(int id) {
        System.out.println("IN PROVIDING...");
    }

    @Override
    public void remove(Flight flight) {
        System.out.println("IN PROVIDING...");
    }
}
