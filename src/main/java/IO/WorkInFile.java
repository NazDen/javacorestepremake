package IO;

import java.io.*;

public class WorkInFile {
    public static void write(String fileName, String text){
        File file = new File(fileName);
        try{
            if(!file.exists()){
                file.createNewFile();
            }
            PrintWriter printWriter = new PrintWriter(file.getAbsoluteFile());
            printWriter.print(text);
            printWriter.close();
        }catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String read(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        StringBuilder stringBuilder = new StringBuilder();
        exists(fileName);
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try{
                String s;
                while ((s = bufferedReader.readLine()) != null){
                    stringBuilder.append(s);
                    stringBuilder.append("\n");
                }
            }finally {
                bufferedReader.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
        return stringBuilder.toString();
    }

    public static void update(String fileName, String newText) throws FileNotFoundException {
        exists(fileName);
        StringBuilder stringBuilder = new StringBuilder();
        String readOldFile = read(fileName);
        stringBuilder.append(readOldFile);
        stringBuilder.append(newText);
        write(fileName, stringBuilder.toString());
    }

    public static void exists(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if(!file.exists()){
            throw new FileNotFoundException(file.getName());
        }
    }
}
