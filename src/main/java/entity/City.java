package entity;


import DAO.Identifiable;


public class City implements Identifiable {
    private final int id;
    private final String name;

    public City(int id,String name) {
        this.id = id;
        this.name = name;
    }

    public City(String raw) {this(raw.split("\t"));}

    public City(String[] raw) { this(raw[0], raw[1]);}

    public City(String id, String nameCity){
        this.id = Integer.parseInt(id);
        this.name = nameCity;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }

    @Override
    public String toText() {
        return String.format("%s\t%s\n", id,name);
    }

    @Override
    public int id() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
