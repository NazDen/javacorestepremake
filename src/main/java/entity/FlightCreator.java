package entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class FlightCreator {

    public List<Flight> createFlights(){
        List<Flight> listOfFlights = new ArrayList<>();

        CityCreator cityCreator = new CityCreator();

        List<City> allCities = cityCreator.createCities();
        System.out.println(allCities);
        AtomicInteger i = new AtomicInteger(100);

        allCities.forEach(city -> allCities.stream()
                .filter(city2 -> city2.id()!= city.id())
                .map(uniqueCities -> {

                    Flight flight = new Flight();
                    long week = 7 * 24 * 60 * 60 * 1000;
                    long twoHours = 2 * 60 * 60 * 1000;
                    long fourHours = 4 * 60 * 60 * 1000;
                    int maxCapacity = 150;

                    flight.setFrom(city.getId());
                    flight.setTo(uniqueCities.getId());
                    flight.setFlightNumber(
                            String.valueOf(city.getName().charAt(0)) +
                            String.valueOf(uniqueCities.getName().charAt(0)) + "-" +
                                    (i.getAndIncrement()));
                    flight.setDate(new Date().getTime() + (long)(Math.random()* week));
                    flight.setDuration(new Date().getTime() + (long) (Math.random() * twoHours) - fourHours);
                    flight.setSeats(maxCapacity - (int) (Math.random() * maxCapacity));
                    return flight;
                })
                .forEach(listOfFlights::add));
        return listOfFlights;
    }

}
