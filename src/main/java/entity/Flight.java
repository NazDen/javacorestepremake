package entity;

import DAO.Identifiable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class Flight implements Identifiable {
    private static int counter = 100;
    private int id;
    private String flightNumber;
    private int from;
    private int to;
    private long date;
    private long duration;
    private int seats;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private SimpleDateFormat durationFormat = new SimpleDateFormat("h'h'mm'm'");
    private SimpleDateFormat defoltFormat = new SimpleDateFormat("dd.MM.yyyy/HH:mm");

    public Flight() {
        this.id = id();
    }

    public Flight(String flightNumber, int from, int to, long date, long duration, int seats) {
        this.id = id();
        this.flightNumber = flightNumber;
        this.from = from;
        this.to = to;
        this.date = date;
        this.duration = duration;
        this.seats = seats;
    }

    public Flight(String raw) {this(raw.split("\t"));}

    public Flight(String[] raw) { this(raw[0], raw[1], raw[2], raw[3], raw[4], raw[5], raw[6], raw[7]);}

    public Flight(String id, String flightNumber, String from, String to, String date, String time, String duration, String seats) {
        this.id = Integer.parseInt(id);
        this.flightNumber = flightNumber;
        this.from = Integer.parseInt(from);
        this.to = Integer.parseInt(to);
        try {
            String timeAndDate = date + "/" + time;
            this.date = defoltFormat.parse(timeAndDate).getTime();
            this.duration = durationFormat.parse(duration).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.seats = Integer.parseInt(seats);
    }

    @Override
    public String toString() {
        return String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\n", id, flightNumber, from, to, dateFormat.format(date),timeFormat.format(date), durationFormat.format(duration), seats);
    }

    @Override
    public int id() {
        return counter++;
    }

    @Override
    public String toText() {
        return String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\n", id, flightNumber, from, to, dateFormat.format(date),timeFormat.format(date), durationFormat.format(duration), seats);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return date == flight.date &&
                Objects.equals(flightNumber, flight.flightNumber) &&
                Objects.equals(from, flight.from) &&
                Objects.equals(to, flight.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightNumber, from, to, date);
    }

    public String getFlightNumber() {
        return flightNumber;
    }


    public long getDate() {
        return date;
    }

    public long getDuration() {
        return duration;
    }

    public int getSeats() {
        return seats;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getId() { return id; }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
