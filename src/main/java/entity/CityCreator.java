package entity;


import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CityCreator {
   public List<City> createCities() {
       AtomicInteger atomicInteger = new AtomicInteger();
       List<City> listOfCities = Stream.of(
               new City(atomicInteger.addAndGet(1), "Kiev"),
               new City(atomicInteger.addAndGet(1), "Rome"),
               new City(atomicInteger.addAndGet(1), "Milan"),
               new City(atomicInteger.addAndGet(1), "Barcelona"),
               new City(atomicInteger.addAndGet(1), "Tallinn"),
               new City(atomicInteger.addAndGet(1), "Minsk"),
               new City(atomicInteger.addAndGet(1), "Warsaw"),
               new City(atomicInteger.addAndGet(1), "Madrid"),
               new City(atomicInteger.addAndGet(1), "Oslo"),
               new City(atomicInteger.addAndGet(1), "Amsterdam"),
               new City(atomicInteger.addAndGet(1), "Paris"),
               new City(atomicInteger.addAndGet(1), "Moscow"),
               new City(atomicInteger.addAndGet(1), "Berlin"),
               new City(atomicInteger.addAndGet(1), "Dnipro"),
               new City(atomicInteger.addAndGet(1), "London"),
               new City(atomicInteger.addAndGet(1), "Stockholm"),
               new City(atomicInteger.addAndGet(1), "Praha"),
               new City(atomicInteger.addAndGet(1), "Munchen"),
               new City(atomicInteger.addAndGet(1), "Monako"),
               new City(atomicInteger.addAndGet(1), "Ankara"),
               new City(atomicInteger.addAndGet(1), "Istanbul")
       ).collect(Collectors.toList());
   return listOfCities;
   }

}
